/**
 * @file
 * A plugin for CKEditor 3.x library
 *
 * This plugin sets the default values for HTML table elements
 * based on values passed to it from its corresponding Drupal module.
 */
(function($) {
	CKEDITOR.plugins.add( 'braindunk_table_defaults', {
		init: function( editor, pluginPath )
		{
			CKEDITOR.on( 'dialogDefinition', function( ev ) {
				// Take the dialog name and its definition from the event data.
				var dialogName = ev.data.name;
				var dialogDefinition = ev.data.definition;
				
				// Check if the definition is from the dialog we're
				// interested on (the "Table" dialog).
				if ( dialogName == 'table' ) {
					// Get a reference to the "Table Info" tab.
					var infoTab = dialogDefinition.getContents( 'info' );
					// Set width
					txtWidth = infoTab.get( 'txtWidth' );
					if(Drupal.settings.wysiwyg_ck_table_default.width_num) {
						var tabWid = Drupal.settings.wysiwyg_ck_table_default.width_num;
					} else {
						var tabWid = "0";
					}
					txtWidth['default'] = tabWid;
					// Set height
					txtHeight = infoTab.get( 'txtHeight' );
					if(Drupal.settings.wysiwyg_ck_table_default.height_num) {
						var tabHei = Drupal.settings.wysiwyg_ck_table_default.height_num;
					} else {
						var tabHei = "0";
					}
					txtHeight['default'] = tabHei;
					// Set border
					txtBorder = infoTab.get( 'txtBorder' );
					if(Drupal.settings.wysiwyg_ck_table_default.border_num) {
						var tabBdr = Drupal.settings.wysiwyg_ck_table_default.border_num;
					} else {
						var tabBdr = "0";
					}
					txtBorder['default'] = tabBdr;
					// Set cellspacing
					txtCspace = infoTab.get( 'txtCellSpace' );
					if(Drupal.settings.wysiwyg_ck_table_default.cspace_num) {
						var tabCsp = Drupal.settings.wysiwyg_ck_table_default.cspace_num;
					} else {
						var tabCsp = "0";
					}
					txtCspace['default'] = tabCsp;
					// Set cellpadding
					txtCpad = infoTab.get( 'txtCellPad' );
					if(Drupal.settings.wysiwyg_ck_table_default.cpad_num) {
						var tabCpd = Drupal.settings.wysiwyg_ck_table_default.cpad_num;
					} else {
						var tabCpd = "0";
					}
					txtCpad['default'] = tabCpd;
					// Set cols
					txtCols = infoTab.get( 'txtCols' );
					if(Drupal.settings.wysiwyg_ck_table_default.cols_num) {
						var tabCol = Drupal.settings.wysiwyg_ck_table_default.cols_num;
					} else {
						var tabCol = "0";
					}
					txtCols['default'] = tabCol;
					// Set rows
					txtRows = infoTab.get( 'txtRows' );
					if(Drupal.settings.wysiwyg_ck_table_default.rows_num) {
						var tabRow = Drupal.settings.wysiwyg_ck_table_default.rows_num;
					} else {
						var tabRow = "0";
					}
					txtRows['default'] = tabRow;
				}
			});
		}
	});
})(jQuery);