
-- SUMMARY --

This module works with Wysiwyg module and provides a plugin for CKEditor that allows
administrators to set the default values for HTML table elements inserted using CKEditor.


For a full description visit the project page:
  http://drupal.org/project/wysiwyg_ck_table_default
Bug reports, feature suggestions and latest developments:
  http://drupal.org/project/issues/wysiwyg_ck_table_default

Brought to you by Braindunk.

-- REQUIREMENTS --

* Wysiwyg


-- INSTALLATION --

* Install as usual, see https://drupal.org/documentation/install/modules-themes/modules-7 for further information.

* Go to Administer > Site configuration > Wysiwyg.

  - Click "edit" to set up your editor profile.

  - Within "Buttons and Plugins" you will see "Table defaults (plugin)" as a new option.

  - Check off "Table defaults (plugin)" to enable setting of new table element defaults.

* Administer > Site configuration > Wysiwyg CKEditor table defaults to set table default values.


-- EDITOR COMPATIBILITY --

Supported editors:

 - CK Editor 3.6.6.1
