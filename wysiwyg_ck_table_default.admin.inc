<?php

/**
 * @file
 * Functionality for Wysiwyg CKEditor Table Defaults administration.
 */

/**
 * Settings form as implemented by hook_menu.
 */
function wysiwyg_ck_table_default_admin_settings($form, &$form_state) {
  //width
  $form['wysiwyg_ck_table_default_width'] = array(
    '#type' => 'textfield',
    '#title' => 'Width',
    '#description' => t('Default width value for HTML table elements. Use integers (without a px unit) or percent sign (%).'),
    '#default_value' => variable_get('wysiwyg_ck_table_default_width', '100%'),
    '#size' => 3,
  );
  //height
  $form['wysiwyg_ck_table_default_height'] = array(
    '#type' => 'textfield',
    '#title' => 'Height',
    '#description' => t('Default height value for HTML table elements. Use integers (without a px unit) or percent sign (%).'),
    '#default_value' => variable_get('wysiwyg_ck_table_default_height', ''),
    '#size' => 3,
  );
  //border
  $form['wysiwyg_ck_table_default_border'] = array(
    '#type' => 'textfield',
    '#title' => 'Border',
    '#description' => t('Default border width (in pixels) for HTML table elements. Use integers.'),
    '#default_value' => variable_get('wysiwyg_ck_table_default_border', ''),
    '#size' => 3,
  );
  //cellspacing
  $form['wysiwyg_ck_table_default_cspace'] = array(
    '#type' => 'textfield',
    '#title' => 'Cell spacing',
    '#description' => t('Default cell spacing (in pixels) for HTML table elements. Use integers.'),
    '#default_value' => variable_get('wysiwyg_ck_table_default_cspace', ''),
    '#size' => 3,
  );
  //cellpadding
  $form['wysiwyg_ck_table_default_cpad'] = array(
    '#type' => 'textfield',
    '#title' => 'Cell padding',
    '#description' => t('Default cell padding (in pixels) for HTML table elements. Use integers.'),
    '#default_value' => variable_get('wysiwyg_ck_table_default_cpad', ''),
    '#size' => 3,
  );
  //columns
  $form['wysiwyg_ck_table_default_cols'] = array(
    '#type' => 'textfield',
    '#title' => 'Columns',
    '#description' => t('Default number of columns for HTML table elements. Use integers.'),
    '#default_value' => variable_get('wysiwyg_ck_table_default_cols', ''),
    '#size' => 3,
  );
  //rows
  $form['wysiwyg_ck_table_default_rows'] = array(
    '#type' => 'textfield',
    '#title' => 'Rows',
    '#description' => t('Default number of rows for HTML table elements. Use integers.'),
    '#default_value' => variable_get('wysiwyg_ck_table_default_rows', ''),
    '#size' => 3,
  );

  // Custom validation to make sure the user is entering numbers.
  $form['#validate'][] = 'wysiwyg_ck_table_default_settings_validate';

  return system_settings_form($form);
}

/**
 * Custom validation for the settings form.
 */
function wysiwyg_ck_table_default_settings_validate($form, &$form_state) {
  //width
  $width = $form_state['values']['wysiwyg_ck_table_default_width'];
  // Check to make sure it is either a number or %-based.
  if (!is_numeric($width)) {
    if (strpos($width, "%"===FALSE)) {
      form_set_error('wysiwyg_ck_table_default_width', t('You must enter a number (without px unit) or a percentage-based value (%).'));
    }
  }
  //height
  $height = $form_state['values']['wysiwyg_ck_table_default_height'];
  // Check to make sure it is either a number or %-based.
  if (!is_numeric($height)) {
    if (strpos($height, "%"===FALSE)) {
      form_set_error('wysiwyg_ck_table_default_height', t('You must enter a number (without px unit) or a percentage-based value (%).'));
    }
  }
  //border
  $brdr = $form_state['values']['wysiwyg_ck_table_default_border'];
  // Check to make sure it is a number.
  if (!is_numeric($brdr)) {
    form_set_error('wysiwyg_ck_table_default_border', t('You must enter an integer.'));
  }
  //cellspacing
  $cspc = $form_state['values']['wysiwyg_ck_table_default_cspace'];
  // Check to make sure it is a number.
  if (!is_numeric($cspc)) {
    form_set_error('wysiwyg_ck_table_default_cspace', t('You must enter an integer.'));
  }
  //cellpadding
  $cpad = $form_state['values']['wysiwyg_ck_table_default_cpad'];
  // Check to make sure it is a number.
  if (!is_numeric($cpad)) {
    form_set_error('wysiwyg_ck_table_default_cpad', t('You must enter an integer.'));
  }
  //cols
  $cols = $form_state['values']['wysiwyg_ck_table_default_cols'];
  // Check to make sure it is a number.
  if (!is_numeric($cols)) {
    form_set_error('wysiwyg_ck_table_default_cols', t('You must enter an integer.'));
  }
  //rows
  $rows = $form_state['values']['wysiwyg_ck_table_default_rows'];
  // Check to make sure it is a number.
  if (!is_numeric($rows)) {
    form_set_error('wysiwyg_ck_table_default_rows', t('You must enter an integer.'));
  }
}
